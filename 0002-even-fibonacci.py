#!/usr/bin/env python3
''' Skriver ut summen av alle par-fibonaccitall under 4 mill '''
import logging
from os import getenv
if getenv('DEBUG') == 'true':
    logging.basicConfig(level='DEBUG')

limit = 4E6

def calc(limit: int) -> int:
    ''' Returnerer summen av par-fibinacci-tallene under "limit" '''
    sum = a = total = 0
    b = 1
    while True:
        sum = a + b
        if sum > limit : break
        logging.debug(f'{a} + {b} = {sum}')
        if sum % 2 == 0:
            total += sum
            logging.debug(f'Total:{total}')
        a = b
        b = sum
    return total

assert calc(2) == 2
assert calc(8) == 10, f'Got {calc(8)}'

print(calc(limit))
