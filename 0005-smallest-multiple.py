#!/usr/bin/env python3
import logging
from os import getenv
if getenv('DEBUG') == 'true':
    logging.basicConfig(level='DEBUG')
# logging.basicConfig(level='DEBUG')

# Brute force... Slow.
def is_evenly_divisible(number: int, upto: int) -> bool:
    for i in range(2, upto + 1):
        if number % i != 0:
            return False
    return True

assert is_evenly_divisible(5, 3) == False
assert is_evenly_divisible(6, 3) == True
assert is_evenly_divisible(24, 4) == True

# print(smallest_number(5))
# print(smallest_number(10))
# print(smallest_number(20)) # 232792560


def smallest_multiple(upto: int):
    numbers = range(upto + 1, 1, -1)
    product = 1
    for num in numbers:
        product *= num
    logging.debug(f'Starter med produkt av alle tallene opp til {upto}: {product}')
    for factor in numbers:
        if is_evenly_divisible((product // factor), upto):
            product = int(product / factor)
            logging.debug(f'Delt på {factor} er fortsatt OK. Nytt produkt: {int(product)}')
        else:
            logging.debug(f'Kan ikke deles på {factor}')
    return int(product)

assert  smallest_multiple(5) == 60
assert smallest_multiple(10) == 2520
print(smallest_multiple(20))
