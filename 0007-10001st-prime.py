#!/usr/bin/env python3
''' Skriv ut det 10001'te primtallet '''
import logging, time
from re import I
from os import getenv
if getenv('DEBUG') == 'true':
    logging.basicConfig(level='DEBUG')
# logging.basicConfig(level='DEBUG')

def is_prime(number: int) -> bool:
    ''' True hvis angitt tall er et primtall, False ellers '''
    for i in range(2, (number // 2) + 1):
        if number % i == 0:
            return False
    return True

assert is_prime(2) == True
assert is_prime(5) == True
assert is_prime(10) == False
assert is_prime(11) == True
assert is_prime(53) == True

def prime_number(number: int) -> int:
    ''' Returnerer primtall nummer 'number'
    F.eks. er 2 det første (returnerer 1) og 11 det femte (returnerer 5)
    '''
    num_primes = 0
    i = 1
    while num_primes < number:
        i += 1
        if is_prime(i):
            num_primes += 1
    return i

assert prime_number(5) == 11
assert prime_number(6) == 13

##########

start_time = time.process_time_ns()
print(f'Prime number 10001 is {prime_number(10001)}')
print(f'{time.process_time_ns() - start_time} nanoseconds')

##########
# 15s processing time
