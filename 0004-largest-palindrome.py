#!/usr/bin/env python3
import logging
from os import getenv
if getenv('DEBUG') == 'true':
    logging.basicConfig(level='DEBUG')
logging.basicConfig(level='DEBUG')

number = 600851475143

def is_palindrome(value: int) -> bool:
    string = str(int(value))
    if string == string[::-1]:
        return True
    return False

def largest_palindrome(digits: int):
    largest = 0
    for i in range(10**digits, 1, -1):
       for j in range(10**digits, 1, -1):
            # logging.debug(f'{i}, {j}, {i*j}')
            if is_palindrome(i * j):
               if i * j > largest:
                   largest = i * j
    return largest

assert is_palindrome('005005') == True
assert is_palindrome('9') == True
assert is_palindrome('99') == True
assert is_palindrome('500') == False
assert is_palindrome('5005') == True

print(largest_palindrome(3))
