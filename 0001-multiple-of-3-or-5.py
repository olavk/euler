#!/usr/bin/env python3
''' Printer ut summen av alle multiplene av 3 og 5 under 1000 '''
import logging
from os import getenv
if getenv('DEBUG') == 'true':
    logging.basicConfig(level='DEBUG')
sum = 0
for i in range(1, 1000):
    logging.debug(f'i:{i}')
    if i % 3 == 0 or i % 5 == 0 :
        sum += i
        logging.debug(f'sum:{sum}')
print(sum)
