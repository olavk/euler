#!/usr/bin/env python3
''' Skriver ut summen av alle primtall under 2M '''
import logging, time
# from re import I
from os import getenv
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s.%(msecs)03d %(levelname)s {%(module)s} [%(funcName)s] %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')

def remove_multiple_of(n: int, candidates: list):
    ''' Setter til False alle multipler av n i angitt liste med bools '''
    # logging.debug(f'Removing multiples of {n} (numbers from {n*2} to {len(candidates)} with step {n}')
    for i in range(n*2, len(candidates), n):
        candidates[i] = False
    return candidates


def sum_primes_below(limit: int) -> int:
    candidates = [True] * (limit + 1)
    candidates[0] = False
    candidates[1] = False
    for to_remove in range(2, ((limit // 2) + 1)):
        candidates = remove_multiple_of(to_remove, candidates)

    sum = 0
    for i in range(len(candidates)):
        if candidates[i] == True:
            sum += i
    logging.debug(f'Sum:{sum}')
    # logging.debug(candidates)
    return sum


assert sum_primes_below(10) == 2+3+5+7
assert sum_primes_below(20) == 77
assert sum_primes_below(100) == 1060
##########

start_time = time.process_time_ns()
print( sum_primes_below(2000000))
print(f'{(time.process_time_ns() - start_time) / 1000000} ms')

# Old: 45597s processing time (12,5h)
# New: 1,7s
# Svar: 142913828922