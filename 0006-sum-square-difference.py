#!/usr/bin/env python3
import logging, time
from os import getenv
if getenv('DEBUG') == 'true':
    logging.basicConfig(level='DEBUG')
# logging.basicConfig(level='DEBUG')

def sum_of_squares(number: int) -> int:
    sum = 0
    for i in range(number+1):
        sum += i**2
    return sum

assert sum_of_squares(3) == 14
assert sum_of_squares(10) == 385

def square_of_sum(number: int) -> int:
    return ((number * (number + 1)) // 2)**2

assert square_of_sum(3) == 36
assert square_of_sum(10) == 3025

##########

start_time = time.process_time_ns()

print(square_of_sum(100) - sum_of_squares(100))

print(f'{time.process_time_ns() - start_time} nanoseconds')

##########
