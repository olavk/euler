#!/usr/bin/env python3
''' Finner alle primtallsfaktorene for et gitt tall, her 600851475143.
Det største er det siste i listen.
'''
import logging
from os import getenv
if getenv('DEBUG') == 'true':
    logging.basicConfig(level='DEBUG')

number = 600851475143

def smallest_prime_factor(value: int) -> int:
    ''' Returnerer den minste primtallsfaktoren for angitt tall'''
    for i in range(2, (value // 2) + 1):
        if (value % i) == 0:
            logging.debug(f'Found factor {i}')
            return i
    else:
        return value

def prime_factors(value: int) -> list:
    factors = list()
    factor = smallest_prime_factor(value)
    factors.append(factor)
    while factor < value:
        value = value // factor
        factor = smallest_prime_factor(value)
        factors.append(factor)
    return factors

assert smallest_prime_factor(2) == 2
assert smallest_prime_factor(9) == 3
assert prime_factors(9) == [3, 3]
assert prime_factors(11) == [11]
assert prime_factors(22) == [2, 11]

print(f'Prime factors for {number} er {prime_factors(number)}')
