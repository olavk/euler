#!/usr/bin/env python3
''' Skriv ut det 10001'te primtallet '''
import logging, time
from re import I
from os import getenv
if getenv('DEBUG') == 'true':
    logging.basicConfig(level='DEBUG')
# logging.basicConfig(level='DEBUG')

def pyt(sum:int) -> int:
    for a in range(1, sum):
        for b in range(a+1, sum):
            for c in range(b+1, sum):
                if a+b+c > sum:
                    break
                if a**2 + b**2 == c**2:
                    if a+b+c == sum:
                        return a*b*c
    return 0

assert pyt(12) == 60


##########

start_time = time.process_time_ns()
print(pyt(1000))
print(f'{time.process_time_ns() - start_time} nanoseconds')

##########
# .002s processing time
